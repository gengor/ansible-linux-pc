# Playbooks for Ubuntu Focal Fossa in Windows WSL #

This playbook is for my Ubuntu Focal Fossa (20.04) setup for my WSL2 in Windows
10.

## Stuff to configure ##

- Settings: overwrite you own stuff? (It won't do a backup!)
- Packages: which packages to install (apt, pip, zip)
- dotfiles: where to get the repo from and which files to link to where, incl. `.config`

