# Playbooks for Ubuntu 22.04 Jammy Jellyfish in WSL #

This playbook is for my Ubuntu 22.04 LTS setup within WSL2.

**‼️ Disclaimer ‼️**

I use these playbooks for my personal setup. There's **no guarantee** that they will work for you. In fact, they will delete or overwrite things on your machine, so only use them at your own risk. Also they may add external repos to your system. Check the [`config.packages.yml`](./config.packages.yml) to see more details on that.

## Prerequisits ##

```shell
sudo apt install ansible 
```

## ✨ Key features ##

- Symlink dotfiles from configurable online repository
- Install base software via apt
- Install pip3-packages
- Use tags to only do specific parts
- Install oh-my-zsh via Github-repo

## 🧰 Stuff to configure ##

- settings: overwrite your own existing stuff? (It won't do a backup!)
- packages: which packages to install (apt, snap) and fonts to install
- dotfiles: which to link to where, incl. .config (will possibly overwrite existing, check settings above)

## ⌨️ Usage

Call the playbook like this:

```shell
sudo ansible-playbook -i inventory main.yml
```

If you just want to do certain parts, you can specify the respective tags like this:

```shell
sudo ansible-playbook -i inventory main.yml --tags apt
```

See the [`main.yml`](./main.yml) for avaiable tags.

Notice the playbook will ask you for which user to do the symlinks for. 

## 🔗 Usefull links ##



