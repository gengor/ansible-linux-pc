# README #

**‼️ Disclaimer ‼️**

I use these playbooks for my personal setup. There's **no guarantee** that they will work for you. In fact, they will delete or overwrite things on your machine, so only use them at your own risk. Also they may add external repos to your system. Check the respective `config.packages.yml` to see more details on that.


These ansible scripts are a collection for setting up my desktop/laptop/wsl2
Linux machines for various use cases and distributions.

Currently available are:

** ✅ tested regularily / should work well **

- [Ubuntu Focal Fossa WSL2](./focal-wsl)
- [Ubuntu 22.04 Jammy Jellyfish](./jammy)
- [Fedora 35](./fedora35)

** ❓ legacy, may not work (well) any more **

- Ubuntu Bionic Beaver via Vagrant (shell only)
- Ubuntu Disco Dingo regular X-Windows install
- Arch Linux (assumes you have X-org already set up)
- Ubuntu Focal Fossa regular X-Windows install

💡 To run them you need to install at least `ansible` manually. See the README inside the indivdual folders for more infos!

