# Notes on my setup

## dconf-settings

### Shell Favorites

`/org/gnome/shell/favorite-apps`

```
['org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop', 'firefox.desktop', 'emacs.desktop', 'org.keepassxc.KeePassXC.desktop', 'com.spotify.Client.desktop', 'org.signal.Signal.desktop', 'org.gnome.Calendar.desktop', 'org.gnome.tweaks.desktop']
```

### App-Picker-Layout

`/org/gnome/shell/app-picker-layout`

```json
[{'microsoft-edge.desktop': <{'position': <0>}>, 'chromium-browser.desktop': <{'position': <1>}>, 'nvim.desktop': <{'position': <2>}>, 'code.desktop': <{'position': <3>}>, 'pycharm-community.desktop': <{'position': <4>}>, 'org.gnome.Calculator.desktop': <{'position': <5>}>, 'org.inkscape.Inkscape.desktop': <{'position': <6>}>, 'gimp.desktop': <{'position': <7>}>, 'com.obsproject.Studio.desktop': <{'position': <8>}>, 'org.shotcut.Shotcut.desktop': <{'position': <9>}>, 'net.sonic_pi.SonicPi.desktop': <{'position': <10>}>, 'org.gnome.Contacts.desktop': <{'position': <11>}>, 'org.gnome.Weather.desktop': <{'position': <12>}>, 'com.skype.Client.desktop': <{'position': <13>}>, 'com.discordapp.Discord.desktop': <{'position': <14>}>, 'gnome-control-center.desktop': <{'position': <15>}>, 'org.gnome.Extensions.desktop': <{'position': <16>}>, 'com.mattjakeman.ExtensionManager.desktop': <{'position': <17>}>, 'ca.desrt.dconf-editor.desktop': <{'position': <18>}>, 'gnome-system-monitor.desktop': <{'position': <19>}>, 'org.gnome.Software.desktop': <{'position': <20>}>}, {'libreoffice-writer.desktop': <{'position': <0>}>, 'libreoffice-calc.desktop': <{'position': <1>}>, 'libreoffice-impress.desktop': <{'position': <2>}>, 'org.kde.krita.desktop': <{'position': <3>}>, 'org.gnome.gitlab.YaLTeR.Identity.desktop': <{'position': <4>}>, 'com.uploadedlobster.peek.desktop': <{'position': <5>}>, 'com.github.huluti.Curtail.desktop': <{'position': <6>}>, 'audacity.desktop': <{'position': <7>}>, 'de.haeckerfelix.Shortwave.desktop': <{'position': <8>}>, 'org.gnome.Maps.desktop': <{'position': <9>}>, 'rhythmbox.desktop': <{'position': <10>}>, 'org.gnome.Totem.desktop': <{'position': <11>}>, 'org.gnome.Photos.desktop': <{'position': <12>}>, 'simple-scan.desktop': <{'position': <13>}>, 'mozilla-thunderbird.desktop': <{'position': <14>}>, 'remote-viewer.desktop': <{'position': <15>}>, 'Alacritty.desktop': <{'position': <16>}>, 'org.gnome.DiskUtility.desktop': <{'position': <17>}>, 'org.gnome.baobab.desktop': <{'position': <18>}>, '02191f83-080d-4f33-954c-36fd01f094ae': <{'position': <19>}>}, {'com.valvesoftware.Steam.desktop': <{'position': <0>}>, 'steam.desktop': <{'position': <1>}>, 'org.gnome.Cheese.desktop': <{'position': <2>}>, 'virt-manager.desktop': <{'position': <3>}>, 'org.gnome.Boxes.desktop': <{'position': <4>}>, 'org.gustavoperedo.FontDownloader.desktop': <{'position': <5>}>, 'org.fedoraproject.MediaWriter.desktop': <{'position': <6>}>, 'teams.desktop': <{'position': <7>}>, 'org.gnome.clocks.desktop': <{'position': <8>}>, 'Utilities': <{'position': <9>}>, 'org.gnome.gedit.desktop': <{'position': <10>}>}, {'htop.desktop': <{'position': <0>}>, 'ranger.desktop': <{'position': <1>}>, 'rxvt-unicode.desktop': <{'position': <2>}>, 'yelp.desktop': <{'position': <3>}>, 'org.gnome.Tour.desktop': <{'position': <4>}>}]
```

### UI

#### GTK: Nordic Darker V40

`/org/gnome/desktop/interface/gtk-theme`

```
'Nordic-darker-v40'
```

#### Icons: Tela

`/org/gnome/desktop/interface/icon-theme`

```
'Tela'
```

#### Shell: Orchis-grey-dark-compact

`/org/gnome/shell/extensions/user-theme`

```
'Orchis-grey-dark-compact'
```


### Nautilus


`/org/gnome/nautilus/preferences/click-policy`

```
'single'
```
