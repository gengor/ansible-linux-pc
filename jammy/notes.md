# App sorting
```
[{'update-manager.desktop': <{'position': <0>}>, 'alacritty_alacritty.desktop': <{'position': <1>}>, 'code_code.desktop': <{'position': <2>}>, 'nvim.desktop': <{'position': <3>}>, 'pycharm-community_pycharm-community.desktop': <{'position': <4>}>, 'org.inkscape.Inkscape.desktop': <{'position': <5>}>, 'gimp.desktop': <{'position': <6>}>, 'audacity.desktop': <{'position': <7>}>, 'emacs.desktop': <{'position': <8>}>, 'thunderbird.desktop': <{'position': <9>}>, 'spotify_spotify.desktop': <{'position': <10>}>, 'shortwave_shortwave.desktop': <{'position': <11>}>, 'org.gnome.Calculator.desktop': <{'position': <12>}>, 'org.gnome.PowerStats.desktop': <{'position': <13>}>, 'gnome-system-monitor.desktop': <{'position': <14>}>, 'ca.desrt.dconf-editor.desktop': <{'position': <15>}>, 'org.gnome.DiskUtility.desktop': <{'position': <16>}>, 'org.gnome.Extensions.desktop': <{'position': <17>}>, 'gnome-control-center.desktop': <{'position': <18>}>, 'org.gnome.tweaks.desktop': <{'position': <19>}>}, {'org.gnome.gedit.desktop': <{'position': <0>}>, 'gnome-language-selector.desktop': <{'position': <1>}>, 'software-properties-gtk.desktop': <{'position': <2>}>, 'Utilities': <{'position': <3>}>}, {'vim.desktop': <{'position': <0>}>, 'emacs-term.desktop': <{'position': <1>}>, 'ranger.desktop': <{'position': <2>}>, 'htop.desktop': <{'position': <3>}>, 'yelp.desktop': <{'position': <4>}>, 'display-im6.q16.desktop': <{'position': <5>}>, 'software-properties-drivers.desktop': <{'position': <6>}>}]
```


## Mozilla PPA


```shell
sudo add-apt-repository ppa:mozillateam/ppa
```

```shell
sudo nvim /etc/apt/preferences.d/mozillateamppa
```

```ini
Package: firefox*
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 501
```
