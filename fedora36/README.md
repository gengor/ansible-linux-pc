# Playbooks for Fedora 36 #

This playbook is for my Fedora 36 desktop setup.

**‼️ Disclaimer ‼️**

I use these playbooks for my personal setup. There's **no guarantee** that they will work for you. In fact, they will delete or overwrite things on your machine, so only use them at your own risk. Also they will add external Microsoft repos to your system, to install Visual Studio Code, Microsoft Teams and PowerShell. Check the [`config.packages.yml`](./config.packages.yml) to see more details on that.

## Prerequisits ##

```shell
sudo dnf install ansible ansible-collection-community-general
```

## ✨ Key features ##

- Symlink dotfiles from configurable online repository
- Install base software via dnf
- Install pip3-packages
- Install better fonts, incl. configurable nerd fonts [Notice: fonts are installed to the system directory]
- Adjust Gnome settings via dconf
- Adds additional repos (e.g. Microsoft)
- Install oh-my-zsh via Github-repo
- Use tags to only do specific parts

![Screenshot my Fedora 36 Gnome Desktop](./fedora-36-screenshot.png)

## 🧰 Stuff to configure ##

- settings: overwrite your own existing stuff? (It won't do a backup!)
- packages: which packages to install (dnf, flatpak) and fonts to install
- dotfiles: which to link to where, incl. .config (will possibly overwrite existing, check settings above)
- dconf: settings to import in to your dconf, e.g. UI fonts

## ⌨️ Usage

Call the playbook like this:

```shell
sudo ansible-playbook -i inventory main.yml
```

If you just want to do certain parts, you can specify the respective tags like this:

```shell
sudo ansible-playbook -i inventory main.yml --tags dnf,fonts
```

See the [`main.yml`](./main.yml) for avaiable tags.

Notice the playbook will ask you for which user to do the symlinks for. Also to this user's directory the fonts will be installed.

## 🔗 Usefull links ##

- [Nice Gnome Terminal colors](https://mayccoll.github.io/Gogh/)
- [Flat Remix](https://github.com/daniruiz/flat-remix) [Alternative](https://drasite.com/flat-remix)

### Gnome shell

Gnome Shell Extensions 

- [Applications Menu](https://extensions.gnome.org/extension/6/applications-menu/)
- [Clipboard Indicator](https://extensions.gnome.org/extension/779/clipboard-indicator/)
- [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/)
- [Hide Top Bar](https://extensions.gnome.org/extension/545/hide-top-bar/)
- [Launch New Instance](https://extensions.gnome.org/extension/600/launch-new-instance/)
- [NetSpeed](https://extensions.gnome.org/extension/104/netspeed/)
- [Removable Drive Menu](https://extensions.gnome.org/extension/7/removable-drive-menu/)
- [Tray Icons: Reloaded](https://extensions.gnome.org/extension/2890/tray-icons-reloaded/)
- [Vitals](https://extensions.gnome.org/extension/1460/vitals/)
- [Workspace Indicator](https://extensions.gnome.org/extension/21/workspace-indicator/)



### GTK Themes

- [Flat Remix GTK](https://www.gnome-look.org/p/1214931)
- [WhiteSur-Nord-Theme](https://www.gnome-look.org/p/1704248)
- [⭐ WhiteSur Gtk Theme](https://www.gnome-look.org/p/1403328)
- [⭐ Orchis gtk theme](https://www.gnome-look.org/p/1357889)
- [⭐ Nordic](https://www.gnome-look.org/p/1267246)
- [Sweet](https://www.gnome-look.org/p/1253385)
- [⭐ Professional Gnome Theme](https://www.gnome-look.org/p/1334194)


### Icons 

- [⭐Tela](https://www.gnome-look.org/p/1279924)
- [Reversal icon theme](https://www.gnome-look.org/p/1340791) 
- [Sweet](thttps://www.pling.com/p/1305251/)
- [Sweet](https://www.opendesktop.org/p/1284047/)
- [Flat Remix](https://www.opendesktop.org/p/1012430)

## 📘 To-Dos ##

-  Gnome extensions
-  Separation of user and system level scripts
-  GTK themes


