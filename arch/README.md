# Ansible Arch Setup #

This Ansible plabook is for setting up my configuration files and installations
for an arch based distribution.

It assumes you have these packages/software already installed and configured:

- git
- X-org
- i3-wm or i3-gaps

Check the config-files for settings and use with caution. Make use of the
`check` flag to see what it __would__ do.
