#!/bin/sh
TEMPLATE_DIR=~/.pandoc/templates
TEMPLATE_FILE=~/.pandoc/templates/eisvogel.latex
if [ ! -d "$TEMPLATE_DIR" ]; then
  echo "Creating pandoc templates dir"
  mkdir $TEMPLATE_DIR -p
else
  echo "Directory already exists"
fi

if [ ! -f "$TEMPLATE_FILE" ]; then
  echo "Downloading template"
  curl -o $TEMPLATE_FILE https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex
else
  echo "Template already exists"
fi

echo "Creating PDF"
pandoc -f markdown -t latex -o README.pdf README.md --template eisvogel -V colorlinks -V titlepage=true -V title="Disco Dingo" -V author="Martin Palmowski" -V titlepage-color=2d3879 -V titlepage-text-color=a4ddea -V titlepage-rule-color=8e6ba4
