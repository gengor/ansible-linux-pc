# Playbooks for Ubuntu Bionic Beaver

This playbook is for my Ubuntu Bionic Beaver (18.04) setup.

![Screenshot Gnome](screenshot01.png)

## Stuff to configure ##

- Settings: overwrite you own stuff? (It won't do a backup!)
- Packages: which packages to install (apt, pip, snap)
- dotfiles: which to link to where, incl. .config
- dconf: settings to import in to your dconf, e.g. UI fonts

## Usefull links ##

- [Nice Gnome Terminal colors](https://mayccoll.github.io/Gogh/)
- [Sierra Mac GTK Theme](https://www.gnome-look.org/p/1013714/)
- [Mojave Mac GTK Theme](https://www.pling.com/p/1275087/)
- [Maia Icons](https://www.opendesktop.org/p/1218961/)
