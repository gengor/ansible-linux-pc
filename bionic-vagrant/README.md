# Ansible Linux PC Setup #

These Ansible scripts are to set up my Linux environment on Ubuntu.

It's currently tested with LTS Version Bionic Beaver 18.04. It should run with
other versions, save for possibly renamed package names.

The playbooks are created to be run locally, but you can use them also remotely
(which I have not tested, tough).

## How to use ##

Notice the settings can be adjusted in `default.config.yml`.

- configure_dotfiles: Install dotfiles from a given git repo
- configure_pandoc: Install Pandoc 2.x and LaTeX tools
- configure_eyecandy: Install more fonts and powerline

You can also see the to-be installed packages there and make adjustments.

- apt_installed_packages
- pandoc_additional_packages

To manually start the playbooks on your machine got to the directory they are
located in and type `ansible-playbook -i inventory main.yml`.

## Vagrant support ##

There's Vagrant file you may use out-of-the-box with VirtualBox. It will install
an official Bionic Beaver image and automatically run the playbooks.

Just take the whole repo, put it in a folder and run `vagrant up` and once
everything has finished connect via `vagrant ssh`. The first provisioning and
booting will take several minutes (up to 20 min).

Keep in mind that if you use Vagrant (specially on Windows) and connect via SSH,
you will need the powerline fonts in your ssh host machine as well. Otherwise
the eyecandy stuff may look awkward.